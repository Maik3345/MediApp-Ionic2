import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, FabContainer, Slides, SegmentButton, ModalController } from 'ionic-angular';

/**
 * Generated class for the HomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  @ViewChild('loopSlider') sliderComponent: Slides;

  public selectedSegment = '1';
  public currentItem: any;
  public listSegments: any = [{
    id: '1',
    name: "Medicamentos",
    icon: null,
    data: [
      {
        name: "Nombre del medicamento",
        detail: "Proxima dosis",
      },
      {
        name: "Nombre del medicamento",
        detail: "Proxima dosis",
      },
      {
        name: "Nombre del medicamento",
        detail: "Proxima dosis",
      }
    ]
  }, {
    id: '2',
    name: "Tips",
    icon: null,
    data: []
  }, {
    id: '3',
    name: "Mitos",
    icon: null,
    data: [
      {
        name: "Nombre del Mito",
        detail: "Proxima dosis",
      },
      {
        name: "Nombre del Mito",
        detail: "Proxima dosis",
      },
      {
        name: "Nombre del Mito",
        detail: "Proxima dosis",
      }
    ]
  },
  {
    id: '4',
    name: "Leyendas",
    icon: null,
    data: []
  }]
  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    // oculto el menu desplegable
    this.menuCtrl.enable(false);
  }
  onSegmentChanged(segmentButton: SegmentButton) {
    if (segmentButton != undefined) {
      console.log('Segment changed to', segmentButton.value);
    }

    const selectedIndex = this.listSegments.findIndex((slide) => {
      if (slide != undefined) {
        return slide.id === segmentButton.value;
      }
    });
    this.sliderComponent.slideTo(selectedIndex);
  }

  onSlideChanged(s: Slides) {
    console.log('Slide changed', s);
    const currentSlide = this.listSegments[s.getActiveIndex()];
    console.log(currentSlide);
    if (currentSlide != undefined) {
      this.currentItem = currentSlide;
      this.selectedSegment = currentSlide.id;
    }
  }

  goToView(page, data?) {
    if (data != null) {
      let info = {
        data: data
      }
      this.navCtrl.push(page, info);
    }
    else {
      this.navCtrl.push(page);
    }
  }
  goToModalView(page, data?) {
    if (data != null) {
      let info = {
        data: data
      }
      let modal = this.modalCtrl.create(page, info);
      modal.onDidDismiss(data => {
        console.log(data);
      });
      modal.present();
    }
    else {
      let modal = this.modalCtrl.create(page);
      modal.onDidDismiss(data => {
        console.log(data);
      });
      modal.present();
    }

  }
  closeFab(fab?: FabContainer) {
    if (fab !== undefined) {
      fab.close();
    }
  }

}
